

:Evaluate: BeginPackage["Wiimote`"]

:Evaluate: ConnectWiimote::usage = "ConnectWiimote[id] tries to pair to the specified wiimote."
:Evaluate: DisconnectAll::usage = "DisconnectAll[] disconnects all active wiimotes. Use with care."

:Evaluate: GetButtonName::usage = "GetButtonName[button] will return the name of the specified button bit."
:Evaluate: GetClassicButtonName::usage = "GetClassicButtonName[button] will return the name of the specified Classic Controller button."
:Evaluate: ButtonPressed::usage = "ButtonPressed[id, button] will return whether the button is pressed."
:Evaluate: ClassicButtonPressed::usage = "ClassicButtonPressed[id, button] will return whether the Classic Controller button is pressed."
:Evaluate: GetJoystickData::usage = "GetJoystickData[id] will return the joystick position (float[4]) if connected.
	If a nunchuk is connected, only first two floats are used. Otherwise, if the extension is a classic controller,
	Guitar Hero Guitar or Drums, it will provide both left and right joystick data as four floats."

:Evaluate: SetLEDs::usage = "SetLEDs[id, leds] will set the LEDs on the wiimote appropriately."
:Evaluate: SetRumble::usage = "SetRumble[id, rumble] will set the state of the wiimote rumble (0 = off)."
:Evaluate: GetBattery::usage = "GetBattery[id] will return the percentage of battery left on the remote."

:Evaluate: GetAcceleration::usage = "GetAcceleration[id] will return the acceleration (float[6]).
	The first three floats will be the wiimote acceleration. The last three will be nunchuk acceleration,
	if one is connected. Otherwise, those accelerations will be 0/0/0."

:Evaluate: GetOrientation::usage = "GetOrientation[id] will return the wiimote orientation. Format is 
	float[UpdateAge, Pitch, Roll, Nunchuk UpdateAge, Nunchuk Pitch, Nunchuk Roll]. If UpdateAge is high,
	data is probably invalid, as orientation can only be estimated when the acceleration is near-zero.
	Nunchuk data only available if that extension is connected. Otherwise, will be zeros for all three."

:Evaluate: GetSampledSection::usage = "GetSampledSection[id, time] will return the wiimote acceleration over time.
	The argument time is in tens of milliseconds, and the format returned will be an array of floats[time * 3], 
	representing the (x, y, z) of each sampled section of time. If any errors occur, the array will fill with the
	update error code (-2 for disconnected, -1 for invalid id, etc.)"

:Evaluate: UnloadSounds::usage = "UnloadSounds[] will unload all loaded sounds and stop all playing music."
:Evaluate: LoadSound::usage = "LoadSound[soundId, path] will pre-load a sound with an identifier."
:Evaluate: PlaySound::usage = "PlaySound[id, soundId] will play a pre-loaded sound on the given wiimote."
:Evaluate: StopSound::usage = "StopSound[id] will turn off the speaker (stop playing) on the given wiimote."

:Evaluate: Begin["`Private`"]

void connectWiimoteWSTP(int);

:Begin:
:Function:       connectWiimoteWSTP
:Pattern:        ConnectWiimote[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void disconnectAllWiimotesWSTP();

:Begin:
:Function:       disconnectAllWiimotesWSTP
:Pattern:        DisconnectAll[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

int buttonPressed(int, int);

:Begin:
:Function:       buttonPressed
:Pattern:        ButtonPressed[id_Integer, button_Integer]
:Arguments:      { id, button }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

void buttonNameWSTP(int);

:Begin:
:Function:       buttonNameWSTP
:Pattern:        GetButtonName[button_Integer]
:Arguments:      { button }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

int classicButtonPressed(int, int);

:Begin:
:Function:       classicButtonPressed
:Pattern:        ClassicButtonPressed[id_Integer, button_Integer]
:Arguments:      { id, button }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Integer
:End:

void classicButtonNameWSTP(int);

:Begin:
:Function:       classicButtonNameWSTP
:Pattern:        GetClassicButtonName[button_Integer]
:Arguments:      { button }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void setLEDsWSTP(int, int);

:Begin:
:Function:       setLEDsWSTP
:Pattern:        SetLEDs[id_Integer, leds_Integer]
:Arguments:      { id, leds }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void setRumbleWSTP(int, int);

:Begin:
:Function:       setRumbleWSTP
:Pattern:        SetRumble[id_Integer, rumble_Integer]
:Arguments:      { id, rumble }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

int getBattery(int);

:Begin:
:Function:       getBattery
:Pattern:        GetBattery[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Integer
:End:

void getAccelerationWSTP(int);

:Begin:
:Function:       getAccelerationWSTP
:Pattern:        GetAcceleration[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void getOrientationWSTP(int);

:Begin:
:Function:       getOrientationWSTP
:Pattern:        GetOrientation[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void getSampledSectionWSTP(int, int);

:Begin:
:Function:       getSampledSectionWSTP
:Pattern:        GetSampledSection[id_Integer, time_Integer]
:Arguments:      { id, time }
:ArgumentTypes:  { Integer, Integer }
:ReturnType:     Manual
:End:

void unloadAllSoundsWSTP();

:Begin:
:Function:       unloadAllSoundsWSTP
:Pattern:        UnloadSounds[]
:Arguments:      { }
:ArgumentTypes:  { }
:ReturnType:     Manual
:End:

void loadSoundWSTP(const char*, const char*);

:Begin:
:Function:       loadSoundWSTP
:Pattern:        LoadSound[soundId_String, path_String]
:Arguments:      { soundId, path }
:ArgumentTypes:  { String, String }
:ReturnType:     Manual
:End:

void playSpeakerSoundWSTP(int, const char*);

:Begin:
:Function:       playSpeakerSoundWSTP
:Pattern:        PlaySound[id_Integer, soundId_String]
:Arguments:      { id, soundId }
:ArgumentTypes:  { Integer, String }
:ReturnType:     Manual
:End:

void stopSpeakerWSTP(int);

:Begin:
:Function:       stopSpeakerWSTP
:Pattern:        StopSound[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:

void getJoystickDataWSTP(int);

:Begin:
:Function:       getJoystickDataWSTP
:Pattern:        GetJoystickData[id_Integer]
:Arguments:      { id }
:ArgumentTypes:  { Integer }
:ReturnType:     Manual
:End:


:Evaluate:  End[ ]
:Evaluate:  EndPackage[ ]