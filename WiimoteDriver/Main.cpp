
#include "..\wiiyourself\wiimote.h"
#include "wstp.h"

#include <map>
#include <mmsystem.h>
#include <iostream>
#include <string>
#include <bitset>
#include <chrono>
#include <thread>
#include <iomanip>

// Map of active wiimotes
std::map<int, wiimote*> activeRemotes;

// Map of loaded sound samples
std::map<std::string, wiimote_sample*> activeSounds;

// ------------------------------------------------------------------------------------
#pragma region Helper Methods

wiimote* getWiimote(int remoteId)
{
	// Search for the remote mapping to the provided ID
	std::map<int, wiimote*>::iterator itr = activeRemotes.find(remoteId);
	wiimote* remote = nullptr;
	// If it exists, set the pointer
	if (itr != activeRemotes.end())
		remote = itr->second;
	return remote;
}

// Thank you to the WiiYourself Demo, this is pretty much wholesale copied from them.
// This is used by the library as a callback. Don't invoke it yourself!
void state_change_helper(wiimote &remote, state_change_flags  changed, const wiimote_state &new_state)
{
	if (changed & CONNECTED)
	{
		// ask the wiimote to report everything (using the 'non-continous updates'
		//  default mode - updates will be frequent anyway due to the acceleration/IR
		//  values changing):
		if (new_state.ExtensionType != wiimote::BALANCE_BOARD)
		{
			if (new_state.bExtension)
				remote.SetReportType(wiimote::IN_BUTTONS_ACCEL_IR_EXT); // no IR dots
			else
				remote.SetReportType(wiimote::IN_BUTTONS_ACCEL_IR);		//    IR dots
		}
	}
	// a MotionPlus was detected
	if (changed & MOTIONPLUS_DETECTED)
	{
		// enable it if there isn't a normal extension plugged into it
		// (MotionPlus devices don't report like normal extensions until
		//  enabled - and then, other extensions attached to it will no longer be
		//  reported (so disable the M+ when you want to access them again).
		if (remote.ExtensionType == wiimote_state::NONE) {
			bool res = remote.EnableMotionPlus();
			_ASSERT(res);
		}
	}
	// an extension is connected to the MotionPlus
	else if (changed & MOTIONPLUS_EXTENSION_CONNECTED)
	{
		// We can't read it if the MotionPlus is currently enabled, so disable it:
		if (remote.MotionPlusEnabled())
			remote.DisableMotionPlus();
	}
	// an extension disconnected from the MotionPlus
	else if (changed & MOTIONPLUS_EXTENSION_DISCONNECTED)
	{
		// enable the MotionPlus data again:
		if (remote.MotionPlusConnected())
			remote.EnableMotionPlus();
	}
	// another extension was just connected:
	else if (changed & EXTENSION_CONNECTED)
	{
		// switch to a report mode that includes the extension data (we will
		//  loose the IR dot sizes)
		// note: there is no need to set report types for a Balance Board.
		if (!remote.IsBalanceBoard())
			remote.SetReportType(wiimote::IN_BUTTONS_ACCEL_IR_EXT);
	}
	// extension was just disconnected:
	else if (changed & EXTENSION_DISCONNECTED)
	{
		// use a non-extension report mode (this gives us back the IR dot sizes)
		remote.SetReportType(wiimote::IN_BUTTONS_ACCEL_IR);
	}
}

// Converts a bitmask to its string representation.
// Much more useful than simply printing the decimal number version.
std::string bitmask_to_str(int bitmask) {
	return std::bitset<16>(bitmask).to_string();
}

// Checks for updates to the remote state (polls the device data)
// Returns -2 if the remote is disconnected.
// Returns -1 if the remote id is invalid.
// Returns 0 otherwise (polling was successful.)
int checkUpdates(int remoteId) {
	wiimote* remote = getWiimote(remoteId);
	// If we couldn't find a wiimote, return false
	if (remote == nullptr)
		return -1;

	// IMPORTANT: the wiimote state needs to be refreshed each pass
	while (remote->RefreshState() == NO_CHANGE) {
		Sleep(1); // don't hog the CPU if nothing changed
	}

	// did we lose the connection?
	if (remote->ConnectionLost()) {
		std::cout << "Disconnected!" << std::endl;
		return -2;
	}

	// Otherwise there's no issues
	return 0;
}

#pragma endregion
// ------------------------------------------------------------------------------------
#pragma region Interface Methods

#pragma region connectWiimote
// Attempts to find and pair to the specified Wiimote. If it can't
// find it within a 1000 cycles, gives up.
// Returns whether the connection succeeded or not.
bool connectWiimote(int number) {
	// Print connection
	std::cout << "Connecting..." << std::endl;
	// Creates a new wiimote for pairing
	wiimote* newRemote = new wiimote();

	// Define the callback so that it reports properly
	newRemote->ChangedCallback = state_change_helper;
	// Notify us only when the wiimote connected sucessfully, or something related to extensions changes
	newRemote->CallbackTriggerFlags = (state_change_flags)(CONNECTED | EXTENSION_CHANGED | MOTIONPLUS_CHANGED);

	// Keep track of the cycles
	int count = 0;
	while (!newRemote->Connect(number)) {
		count++;
		if (count >= 1000)
			break;
	}
	// If we're not connected after a thousand cycles
	if (!newRemote->IsConnected()) {
		std::cout << "Failed to connect!" << std::endl;
		return false;
	}
	// Active all LEDs as a visual indicator
	newRemote->SetLEDs(0x0f);
	// Set it in the map
	activeRemotes[number] = newRemote;
	// Return the successful connection
	std::cout << "Connected successfully!" << std::endl;
	return true;
}

// WSTP helper function.
void connectWiimoteWSTP(int number) {
	// Helper string of the number
	std::string num = std::to_string(number);
	std::string str = connectWiimote(number) // Try to connect
		? "Successfully connected to Wiimote #" + num // Successful
		: "Failed to find a connection to Wiimote #" + num; // Failure
	// Send the string up to Mathematica
	WSPutString(stdlink, &str[0]);
}
#pragma endregion

#pragma region disconnectAll
// Invokes a forcible disconnect on all wiimotes.
// Takes no parameters, and clears all valid IDs. Use with care.
void disconnectAllWiimotes() {
	std::cout << "Disconnecting..." << std::endl;
	// Loop through all active wii remotes
	for (auto it = activeRemotes.begin(); it != activeRemotes.end(); ++it) {
		// Stop any active LEDs just to be safe
		it->second->SetLEDs(0x00);
		// Stop any rumbling just in case
		it->second->SetRumble(false);
		// Stop any music on that off chance
		it->second->EnableSpeaker(false);
		// Invoke disconnect
		it->second->Disconnect();
	}
	// Clear the active id map
	activeRemotes.clear();
	std::cout << "Done!" << std::endl;
}

// WSTP helper function.
void disconnectAllWiimotesWSTP() {
	// Really, this exists so that we can debug
	// in Visual Studio w/o needing to link to Mathematica.
	disconnectAllWiimotes();
	WSPutString(stdlink, "Disconnected all wiimotes.");
}
#pragma endregion

#pragma region buttons
// Checks the state of the provided button bit.
// Button bits are in order:
// { Left, Right, Down, Up, Plus, C, Z, ??, Two, One, B, A, Minus, ??, ??, Home }
// 'C' and 'Z' only are prevalant if the Nunchuk is connected.
// Returns -2 if the remote is disconnected.
// Returns -1 if the remote id is invalid.
// Returns 0 if the button is not pressed.
// returns 1 if the button is pressed.
int buttonPressed(int remoteId, int buttonBit) {
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0)
		return update;
	// We know that the remote exists here
	wiimote* remote = getWiimote(remoteId);
	// Custom assignment bits
	if (buttonBit == 5 || buttonBit == 6) {
		// If the nunchuk is connected
		if (remote->ExtensionType == wiimote_state::NUNCHUK) {
			return buttonBit == 5 // Which bit is it?
				? remote->Nunchuk.C // 5 == "C"
				: remote->Nunchuk.Z; // 6 == "Z"
		}
		// Otherwise, let it filter down.
	}
	// Get the bitmask of the provided bit
	WORD mask = (WORD)(1 << buttonBit);
	// Return whether the specified button bit is pressed
	return (remote->Button.Bits & mask) != 0;
}

// Checks the state of the provided classic controller button bit.
// Button bits are in order:
// { ??, TrigR, Plus, Home, Minus, TrigL, Down, Right, Up, Left, ZR, X, A, Y, B, ZL }
// Returns -3 if a classic controller is not connected.
// Returns -2 if the remote is disconnected.
// Returns -1 if the remote id is invalid.
// Returns 0 if the button is not pressed.
// returns 1 if the button is pressed.
int classicButtonPressed(int remoteId, int buttonBit) {
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0)
		return update;
	// We know that the remote exists here
	wiimote* remote = getWiimote(remoteId);
	// Is a classic controller connected
	if (remote->ExtensionType == wiimote_state::CLASSIC ||
		remote->ExtensionType == wiimote_state::GH3_GHWT_GUITAR ||
		remote->ExtensionType == wiimote_state::GHWT_DRUMS) {
		// Classic Controller, Guitar Hero, etc. are the same

		// Get the bitmask of the provided bit
		WORD mask = (WORD)(1 << buttonBit);
		// Return whether the specified button bit is pressed
		return (remote->ClassicController.Button.Bits & mask) != 0;
	}
	// Otherwise, return
	return -3;
}


// WSTP helper function.
void buttonNameWSTP(int buttonBit) {
	if (buttonBit < 0 || buttonBit >= TOTAL_BUTTON_BITS) {
		std::string bit = std::to_string(buttonBit);
		std::string str = "Invalid bit " + bit + "!";
		WSPutString(stdlink, &str[0]);
		return;
	}
	// Custom assignment bits
	if (buttonBit == 5 || buttonBit == 6) {
		std::string str = buttonBit == 5 ? "C" : "Z";
		WSPutString(stdlink, &str[0]);
		return;
	}
	WSPutString(stdlink, wiimote::ButtonNameFromBit[buttonBit]);
}

// WSTP helper function.
void classicButtonNameWSTP(int buttonBit) {
	if (buttonBit < 0 || buttonBit >= TOTAL_BUTTON_BITS) {
		std::string bit = std::to_string(buttonBit);
		std::string str = "Invalid bit " + bit + "!";
		WSPutString(stdlink, &str[0]);
		return;
	}
	WSPutString(stdlink, wiimote::ClassicButtonNameFromBit[buttonBit]);
}
#pragma endregion

#pragma region LEDs
// Sets the LEDs on the provided wiimote.
// The led_mask argument is a bitmask (0000 -> 1111).
// Returns -1 if the remote id is invalid.
// Returns 0 otherwise.
int setLEDs(int remoteId, int led_mask) {
	wiimote* remote = getWiimote(remoteId);
	// If we couldn't find a wiimote, return false
	if (remote == nullptr)
		return -1;
	// Helper to_string of the led mask
	std::string num = bitmask_to_str(led_mask).substr(12);
	// Print out out
	std::cout << "Setting LEDs to " << num << std::endl;
	// Set the LED by bitmask
	remote->SetLEDs(led_mask);
	return 0;
}

// WSTP helper function.
void setLEDsWSTP(int remoteId, int led_mask) {
	// Helper to_string of the remote ID
	std::string id = std::to_string(remoteId);
	// Helper to_string of the led mask
	std::string num = bitmask_to_str(led_mask).substr(12);
	// Try and build the string
	std::string str = setLEDs(remoteId, led_mask) == -1 // Try and set LEDs
		? "Invalid remote id #" + id + "!" // Invalid id
		: "Set LEDs to " + num + "."; // Nothing went wrong
	WSPutString(stdlink, &str[0]);
}
#pragma endregion

#pragma region rumble
// Sets the rumble on the provided wiimote.
// Returns -1 if the remote id is invalid.
// Returns 0 otherwise.
int setRumble(int remoteId, bool rumble) {
	wiimote* remote = getWiimote(remoteId);
	// If we couldn't find a wiimote, return false
	if (remote == nullptr)
		return -1;
	std::cout << "Turning Rumble " << (rumble ? "On" : "Off") << std::endl;
	remote->SetRumble(rumble);
	return 0;
}

// WSTP helper function.
void setRumbleWSTP(int remoteId, int rumble) {
	// Helper to_string of the remote ID
	std::string id = std::to_string(remoteId);
	// Helper string of the rumble state.
	std::string val = (rumble ? "On" : "Off");
	// Try and build the string
	std::string str = setRumble(remoteId, rumble) == -1 // Try and set LEDs
		? "Invalid remote id #" + id + "!" // Invalid id
		: "Turned Rumble " + val + "."; // Nothing went wrong
	WSPutString(stdlink, &str[0]);
}
#pragma endregion

#pragma region battery
// Check the battery percentage of the wiimote.
// Returns -2 if the remote is disconnected.
// Returns -1 if the remote id is invalid.
// Returns 0-100 (a percentage) otherwise.
int getBattery(int remoteId) {
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0)
		return update;
	// We know that the remote exists here
	wiimote* remote = getWiimote(remoteId);
	// Return the battery percentage
	return remote->BatteryRaw;
}
#pragma endregion

#pragma region acceleration
// Gets the acceleration of the wiimote.
// Returns [-2, ...] if the remote is disconnected.
// Returns [-1, ...] if the remote id is invalid.
// Returns float[6] of acceleration otherwise.
// The last three floats will be the nunchuk data, if connected.
float* getAcceleration(int remoteId) {
	// Make the return array
	float accelArr[6] = { 0, 0, 0, 0, 0, 0 };
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0) {
		for (int x = 0; x < 6; x++)
			accelArr[x] = (float)update;
	}
	// Otherwise, get the acceleration
	else {
		// We know that the remote exists here
		wiimote* remote = getWiimote(remoteId);
		accelArr[0] = remote->Acceleration.X;
		accelArr[1] = remote->Acceleration.Y;
		accelArr[2] = remote->Acceleration.Z;
		// Does the nunchuk exist?
		if (remote->ExtensionType == wiimote_state::NUNCHUK) {
			accelArr[3] = remote->Nunchuk.Acceleration.X;
			accelArr[4] = remote->Nunchuk.Acceleration.Y;
			accelArr[5] = remote->Nunchuk.Acceleration.Z;
		}
	}
	return accelArr;
}

// WSTP helper function.
void getAccelerationWSTP(int remoteId) {
	// Invoke the method
	float* arr = getAcceleration(remoteId);
	// Send it up to Mathematica
	// WHY IS THIS NOT DOCUMENTED???
	WSPutReal32List(stdlink, arr, 6);
}
#pragma endregion

#pragma region orientation
// Gets the estimated orientation of the wiimote.
// Returns [-2, ...] if the remote is disconnected.
// Returns [-1, ...] if the remote id is invalid.
// Returns float[6] of orientation otherwise.
// Format is [UpdateAge, Pitch, Roll, Nunchuk UpdateAge, Nunchuk Pitch, Nunchuk Roll]
float* getOrientation(int remoteId) {
	// Make the return array
	float orientArr[6] = { 0, 0, 0, 0, 0, 0 };
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0) {
		for (int x = 0; x < 6; x++)
			orientArr[x] = (float)update;
	}
	// Otherwise, get the orientation
	else {
		// We know that the remote exists here
		wiimote* remote = getWiimote(remoteId);
		orientArr[0] = (float)remote->Acceleration.Orientation.UpdateAge;
		orientArr[1] = remote->Acceleration.Orientation.Pitch;
		orientArr[2] = remote->Acceleration.Orientation.Roll;
		// Does the nunchuk exist?
		if (remote->ExtensionType == wiimote_state::NUNCHUK) {
			orientArr[3] = remote->Nunchuk.Acceleration.Orientation.UpdateAge;
			orientArr[4] = remote->Nunchuk.Acceleration.Orientation.Pitch;
			orientArr[5] = remote->Nunchuk.Acceleration.Orientation.Roll;
		}
	}
	return orientArr;
}

// WSTP helper function.
void getOrientationWSTP(int remoteId) {
	// Invoke the method
	float* arr = getOrientation(remoteId);
	// Send it up to Mathematica
	// WHY IS THIS NOT DOCUMENTED???
	WSPutReal32List(stdlink, arr, 6);
}
#pragma endregion

#pragma region timeSection

// Gets N samples of orientation. (1 sample = 10 milliseconds, roughly)
// Returns [-2] if the remote is disconnected.
// Returns [-1] if the remote id is invalid.
// Returns float[time*3] of orientation-per-sample otherwise.
// Format is [X, Y, Z, ...]
float* getSampledSection(int remoteId, int time) {
	// Make the array
	float* samples = new float[time * 3];
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0) {
		samples[0] = (float)update;
	}
	// Otherwise, get the orientation
	else {
		// We know that the remote exists here
		wiimote* remote = getWiimote(remoteId);
		for (int n = 0; n < time; n++) {
			checkUpdates(remoteId);
			samples[n * 3 + 0] = remote->Acceleration.X;
			samples[n * 3 + 1] = remote->Acceleration.Y;
			samples[n * 3 + 2] = remote->Acceleration.Z;
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
	return samples;
}

// WSTP helper function.
void getSampledSectionWSTP(int remoteId, int time) {
	std::cout << "Polling..." << std::endl;
	// Invoke the method
	float* arr = getSampledSection(remoteId, time);
	// Send it up to Mathematica
	// WHY IS THIS NOT DOCUMENTED???
	WSPutReal32List(stdlink, arr, time * 3);
	std::cout << "Sending " << time << " samples up." << std::endl;
}
#pragma endregion

#pragma region sounds
// Unloads all loaded sounds and stops all playing speakers.
// Takes no arguments, and clears the map. Use with care.
void unloadAllSounds() {
	std::cout << "Unloading..." << std::endl;
	// Loop through all active wii remotes
	for (auto it = activeRemotes.begin(); it != activeRemotes.end(); ++it) {
		// Stop the music
		it->second->EnableSpeaker(false);
	}
	// Clear the map - the destructors will handle it
	activeSounds.clear();
	std::cout << "Done!" << std::endl;
}

// WSTP helper function
void unloadAllSoundsWSTP() {
	// Really, this exists so that we can debug
	// in Visual Studio w/o needing to link to Mathematica.
	unloadAllSounds();
	WSPutString(stdlink, "Unloaded all sounds.");
}


// Attempts to load the sound and store it with the specified ID.
// The filepath must point to a 16-bit mono-sample WAV [2470 to 4200 Hz].
// Returns -2 if the ID is already used.
// Returns -1 if the sound cannot be found.
// Returns 0 otherwise.
int loadSound(std::string soundIdentifier, std::string filepath) {
	// If the ID exists, don't overwrite
	if (activeSounds.count(soundIdentifier) == 1)
		return -2;
	// Make a new sample
	wiimote_sample* sample = new wiimote_sample();
	std::cout << "Loading sample \"" << filepath << "\" as \"" << soundIdentifier << "\"..." << std::endl;
	// If loading the sound file fails
	if (!wiimote::Load16bitMonoSampleWAV(_T(&filepath[0]), *sample)) {
		std::cout << "Failed to find file!" << std::endl;
		return -1;
	}
	std::cout << "Loaded file!" << std::endl;
	// Store it in the map
	activeSounds[soundIdentifier] = sample;
	// Return
	return 0;
}

// WSTP helper function
void loadSoundWSTP(const char* soundIdentifier, const char* filepath) {
	// Helper string of the sound ID
	std::string id(soundIdentifier);
	// Helper string of the filepath
	std::string path(filepath);
	// Try and load sound
	int response = loadSound(id, path);
	// Try and build the string
	std::string str = "Loaded sound \"" + path + "\" as \"" + id + "\".";
	if (response == -2) // Invalid sound id
		str = "Sound with id \"" + id + "\" already exists!";
	if (response == -1) // Invalid filepaht
		str = "Cannot find sound at filepath \"" + path + "\"!";
	// Send to mathematica
	WSPutString(stdlink, &str[0]);
}


// Attempts to play the pre-loaded sound on the Wiimote speaker.
// Returns -3 if the sound is not loaded.
// Returns -2 if the remote is disconnected.
// Returns -1 if the remote id is invalid.
// Returns 0 otherwise.
int playSpeakerSound(int remoteId, std::string soundIdentifier) {
	// If the ID isn't loaded, exit
	if (activeSounds.count(soundIdentifier) == 0)
		return -3;
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0)
		return update;
	// We know that the remote exists here
	wiimote* remote = getWiimote(remoteId);
	// Get the relevant sound sample
	wiimote_sample* sample = activeSounds[soundIdentifier];
	// Turn off the speakers to stop currently playing songs
	remote->EnableSpeaker(false);
	// Turn on the speakers now that they're 'reset'
	remote->EnableSpeaker(true);
	// Play the song
	remote->PlaySample(*sample);
	// Print
	std::cout << "Playing " << soundIdentifier << "!" << std::endl;
	// Return
	return 0;
}

// WSTP helper function
void playSpeakerSoundWSTP(int remoteId, const char* soundIdentifier) {
	// Helper to_string of the remote ID
	std::string id = std::to_string(remoteId);
	// Helper to_string of the led mask
	std::string sound(soundIdentifier);
	// Try and load sound
	int response = playSpeakerSound(remoteId, sound);
	// Try and build the string
	std::string str = "Remote #" + id + " playing \"" + sound + "\".";
	if (response == -3) // Invalid sound id
		str = "Sound with id \"" + id + "\" not loaded!";
	if (response == -2) // Remote disconnected
		str = "Remote #" + id + " is disconnected!";
	if (response == -1) // Invalid remote ID
		str = "Invalid remote id #" + id + "!";
	// Send to mathematica
	WSPutString(stdlink, &str[0]);
}


// Attempts to stop any currently playing song on the Wiimote speaker.
// Returns -1 if the remote id is invalid.
// Returns 0 otherwise.
int stopSpeaker(int remoteId) {
	// Try and get the wiimote
	wiimote* remote = getWiimote(remoteId);
	// If we couldn't find a wiimote, return false
	if (remote == nullptr)
		return -1;
	// Turn off the speakers
	remote->EnableSpeaker(false);
	return 0;
}

// WSTP helper function
void stopSpeakerWSTP(int remoteId) {
	// Helper to_string of the remote ID
	std::string id = std::to_string(remoteId);
	// Try and build the string
	std::string str = stopSpeaker(remoteId) == -1 // Try and stop speaker
		? "Invalid remote id #" + id + "!" // Invalid id
		: "Stopped wiimote speaker."; // Nothing went wrong
	WSPutString(stdlink, &str[0]);
}

#pragma endregion

#pragma region joystick
// Get Joystick Position, if possible.
// If the extension is a nunchuck, only first two floats are used.
// If a Classic Controller (et. al), returns four floats.
// Returns [-3, ...] if the nunchuck is not connected
// Returns [-2, ...] if the remote is disconnected.
// Returns [-1, ...] if the remote id is invalid.
// Returns float[4] of joystick data otherwise.
float* getJoystickData(int remoteId) {
	// Make the return array
	float joystickArr[4] = { 0, 0, 0, 0 };
	// Make sure to check for updates
	int update = checkUpdates(remoteId);
	// If it's disconnected or invalid
	if (update != 0) {
		for (int x = 0; x < 4; x++)
			joystickArr[x] = (float)update;
	}
	// Otherwise, check the extension
	else {
		// We know that the remote exists here
		wiimote* remote = getWiimote(remoteId);
		// But does the nunchuk exist?
		if (remote->ExtensionType == wiimote_state::NUNCHUK) {
			joystickArr[0] = remote->Nunchuk.Joystick.X;
			joystickArr[1] = remote->Nunchuk.Joystick.Y;
		}
		else if ( // Classic Controller, Guitar Hero, etc. are the same
			remote->ExtensionType == wiimote_state::CLASSIC ||
			remote->ExtensionType == wiimote_state::GH3_GHWT_GUITAR ||
			remote->ExtensionType == wiimote_state::GHWT_DRUMS) {
			joystickArr[0] = remote->ClassicController.JoystickL.X;
			joystickArr[1] = remote->ClassicController.JoystickL.Y;
			joystickArr[2] = remote->ClassicController.JoystickR.X;
			joystickArr[3] = remote->ClassicController.JoystickR.Y;
		}
		// Nothing is connected
		else {
			for (int x = 0; x < 4; x++)
				joystickArr[x] = -3.0f;
		}
	}
	return joystickArr;
}

// WSTP helper function.
void getJoystickDataWSTP(int remoteId) {
	// Invoke the method
	float* arr = getJoystickData(remoteId);
	// Send it up to Mathematica
	// WHY IS THIS NOT DOCUMENTED???
	WSPutReal32List(stdlink, arr, 4);
}
#pragma endregion

#pragma endregion
// ------------------------------------------------------------------------------------
#pragma region Main Methods

#if !USE_DEMO
#if DEBUG

int main()
{
	int numId = 1;

	if (loadSound("Solemn", "Our Solemn Hour.wav") != 0) {
		std::cin.get();
		return 1;
	}

reconnect:
	if (!connectWiimote(numId)) {
		std::cout << "Could not find wiimote..." << std::endl;
		std::cin.get();
		goto reconnect;
		//return 1;
	}

	DWORD last_led_time = timeGetTime();
	int numLED = 0;
	bool playing = false;

	while (buttonPressed(numId, 15) != 1) {
		int poll = checkUpdates(numId);
		if (poll < 0) {
			std::cout << "Disconnected!" << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			goto reconnect;
		}
		DWORD current_time = timeGetTime();
		if ((current_time - last_led_time) >= 500) {
			numLED = (++numLED) % 4;
			//setLEDs(numId, (BYTE)(1 << numLED));
			last_led_time = timeGetTime();
		}
		if (!playing && buttonPressed(numId, 9) == 1) {
			playSpeakerSound(numId, "Solemn");
			playing = true;
		}
		if (buttonPressed(numId, 8) == 1) {
			stopSpeaker(numId);
			playing = false;
		}
		if (buttonPressed(numId, 11) == 1) {
			float* samples = getSampledSection(numId, 100);
			for (int x = 0; x < 3; x++) {
				std::cout << (x == 0 ? "X" : x == 1 ? "Y" : "Z") << ": ";
				for (int y = 0; y < 100; y++) {
					std::cout << std::setprecision(2) << samples[y * 2 + x] << " ";
				}
				std::cout << std::endl;
			}
		}
		if (buttonPressed(numId, 10) == 1) {
			float* samples = getAcceleration(numId);
			for (int y = 0; y < 6; y++) {
				std::cout << std::setprecision(2) << samples[y] << " ";
			}
			std::cout << std::endl;
		}
	}
	disconnectAllWiimotes();
	unloadAllSounds();
	std::cin.get();
	return 0;
}

#else

int main(int argc, char **argv)
{
	int ret = WSMain(argc, argv);
	disconnectAllWiimotes();
	unloadAllSounds();
	return ret;
}

#endif
#endif

#pragma endregion