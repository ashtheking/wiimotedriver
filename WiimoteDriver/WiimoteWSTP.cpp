/*
 * This file automatically produced by C:\Program Files\Wolfram Research\Mathematica\11.3\SystemFiles\Links\WSTP\DeveloperKit\Windows-x86-64\CompilerAdditions\wsprep.exe from:
 *	Wiimote.tm
 * mprep Revision 18 Copyright (c) Wolfram Research, Inc. 1990-2013
 */

#define MPREP_REVISION 18


#include "wstp.h"

int PREPAbort = 0;
int PREPDone  = 0;
long PREPSpecialCharacter = '\0';
HANDLE WSInstance = (HANDLE)0;
HWND WSIconWindow = (HWND)0;

WSLINK stdlink = 0;
WSEnvironment stdenv = 0;
WSYieldFunctionObject stdyielder = (WSYieldFunctionObject)0;
WSMessageHandlerObject stdhandler = (WSMessageHandlerObject)0;

extern int PREPDoCallPacket(WSLINK);
extern int PREPEvaluate( WSLINK, char *);
extern int PREPEvaluateString P(( WSLINK, char *));

#include <windows.h>

#if defined(__GNUC__)

#	ifdef TCHAR
#		undef TCHAR
#	endif
#	define TCHAR char

#	ifdef PTCHAR
#		undef PTCHAR
#	endif
#	define PTCHAR char *

#	ifdef __TEXT
#		undef __TEXT
#	endif
#	define __TEXT(arg) arg

#	ifdef _tcsrchr
#		undef _tcsrchr
#	endif
#	define _tcsrchr strrchr

#	ifdef _tcscat
#		undef _tcscat
#	endif
#	define _tcscat strcat

#	ifdef _tcsncpy
#		undef _tcsncpy
#	endif
#	define _tcsncpy _fstrncpy
#else
#	include <tchar.h>
#endif

#include <stdlib.h>
#include <string.h>
#if (WIN32_MATHLINK || WIN64_MATHLINK || __GNUC__) && !defined(_fstrncpy)
#       define _fstrncpy strncpy
#endif

#ifndef CALLBACK
#define CALLBACK FAR PASCAL
typedef LONG LRESULT;
typedef unsigned int UINT;
typedef WORD WPARAM;
typedef DWORD LPARAM;
#endif


LRESULT CALLBACK WSEXPORT
IconProcedure( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK WSEXPORT
IconProcedure( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch( msg){
	case WM_CLOSE:
		PREPDone = 1;
		PREPAbort = 1;
		break;
	case WM_QUERYOPEN:
		return 0;
	}
	return DefWindowProc( hWnd, msg, wParam, lParam);
}


#ifdef _UNICODE
#define _APISTR(i) L ## #i
#else
#define _APISTR(i) #i
#endif

#define APISTR(i) _APISTR(i)

HWND WSInitializeIcon( HINSTANCE hInstance, int nCmdShow)
{
	TCHAR path_name[260];
	PTCHAR icon_name;

	WNDCLASS  wc;
	HMODULE hdll;

	WSInstance = hInstance;
	if( ! nCmdShow) return (HWND)0;

	hdll = GetModuleHandle( __TEXT("wstp32i" APISTR(WSINTERFACE)));

	(void)GetModuleFileName( hInstance, path_name, 260);

	icon_name = _tcsrchr( path_name, '\\') + 1;
	*_tcsrchr( icon_name, '.') = '\0';


	wc.style = 0;
	wc.lpfnWndProc = IconProcedure;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;

	if( hdll)
		wc.hIcon = LoadIcon( hdll, __TEXT("WSIcon"));
	else
		wc.hIcon = LoadIcon( NULL, IDI_APPLICATION);

	wc.hCursor = LoadCursor( NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)( COLOR_WINDOW + 1);
	wc.lpszMenuName =  NULL;
	wc.lpszClassName = __TEXT("mprepIcon");
	(void)RegisterClass( &wc);

	WSIconWindow = CreateWindow( __TEXT("mprepIcon"), icon_name,
			WS_OVERLAPPEDWINDOW | WS_MINIMIZE, CW_USEDEFAULT,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
			(HWND)0, (HMENU)0, hInstance, (void FAR*)0);

	if( WSIconWindow){
		ShowWindow( WSIconWindow, SW_MINIMIZE);
		UpdateWindow( WSIconWindow);
	}
	return WSIconWindow;
}

int PREPDefaultYielder( WSLINK wslp, WSYieldParameters yp)
{
	MSG msg;
	wslp = wslp; /* suppress unused warning */
	yp = yp; /* suppress unused warning */

	if( PeekMessage( &msg, (HWND)0, 0, 0, PM_REMOVE)){
		TranslateMessage( &msg);
		DispatchMessage( &msg);
	}
	return PREPDone;
}


/********************************* end header *********************************/


# line 41 "Wiimote.tm"
void connectWiimoteWSTP(int);

# line 174 "WiimoteWSTP.cpp"


# line 51 "Wiimote.tm"
void disconnectAllWiimotesWSTP();

# line 180 "WiimoteWSTP.cpp"


# line 61 "Wiimote.tm"
int buttonPressed(int, int);

# line 186 "WiimoteWSTP.cpp"


# line 71 "Wiimote.tm"
void buttonNameWSTP(int);

# line 192 "WiimoteWSTP.cpp"


# line 81 "Wiimote.tm"
int classicButtonPressed(int, int);

# line 198 "WiimoteWSTP.cpp"


# line 91 "Wiimote.tm"
void classicButtonNameWSTP(int);

# line 204 "WiimoteWSTP.cpp"


# line 101 "Wiimote.tm"
void setLEDsWSTP(int, int);

# line 210 "WiimoteWSTP.cpp"


# line 111 "Wiimote.tm"
void setRumbleWSTP(int, int);

# line 216 "WiimoteWSTP.cpp"


# line 121 "Wiimote.tm"
int getBattery(int);

# line 222 "WiimoteWSTP.cpp"


# line 131 "Wiimote.tm"
void getAccelerationWSTP(int);

# line 228 "WiimoteWSTP.cpp"


# line 141 "Wiimote.tm"
void getOrientationWSTP(int);

# line 234 "WiimoteWSTP.cpp"


# line 151 "Wiimote.tm"
void getSampledSectionWSTP(int, int);

# line 240 "WiimoteWSTP.cpp"


# line 161 "Wiimote.tm"
void unloadAllSoundsWSTP();

# line 246 "WiimoteWSTP.cpp"


# line 171 "Wiimote.tm"
void loadSoundWSTP(const char*, const char*);

# line 252 "WiimoteWSTP.cpp"


# line 181 "Wiimote.tm"
void playSpeakerSoundWSTP(int, const char*);

# line 258 "WiimoteWSTP.cpp"


# line 191 "Wiimote.tm"
void stopSpeakerWSTP(int);

# line 264 "WiimoteWSTP.cpp"


# line 201 "Wiimote.tm"
void getJoystickDataWSTP(int);

# line 270 "WiimoteWSTP.cpp"


void connectWiimoteWSTP P(( int _tp1));

static int _tr0( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	connectWiimoteWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr0 */


void disconnectAllWiimotesWSTP P(( void));

static int _tr1( WSLINK wslp)
{
	int	res = 0;
	if ( ! WSNewPacket(wslp) ) goto L0;
	if( !wslp) return res; /* avoid unused parameter warning */

	disconnectAllWiimotesWSTP();

	res = 1;

L0:	return res;
} /* _tr1 */


int buttonPressed P(( int _tp1, int _tp2));

static int _tr2( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _tp2;
	int _rp0;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	_rp0 = buttonPressed(_tp1, _tp2);

	res = PREPAbort ?
		WSPutFunction( wslp, "Abort", 0) : WSPutInteger( wslp, _rp0);
L2: L1: 
L0:	return res;
} /* _tr2 */


void buttonNameWSTP P(( int _tp1));

static int _tr3( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	buttonNameWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr3 */


int classicButtonPressed P(( int _tp1, int _tp2));

static int _tr4( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _tp2;
	int _rp0;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	_rp0 = classicButtonPressed(_tp1, _tp2);

	res = PREPAbort ?
		WSPutFunction( wslp, "Abort", 0) : WSPutInteger( wslp, _rp0);
L2: L1: 
L0:	return res;
} /* _tr4 */


void classicButtonNameWSTP P(( int _tp1));

static int _tr5( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	classicButtonNameWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr5 */


void setLEDsWSTP P(( int _tp1, int _tp2));

static int _tr6( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _tp2;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	setLEDsWSTP(_tp1, _tp2);

	res = 1;
L2: L1: 
L0:	return res;
} /* _tr6 */


void setRumbleWSTP P(( int _tp1, int _tp2));

static int _tr7( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _tp2;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	setRumbleWSTP(_tp1, _tp2);

	res = 1;
L2: L1: 
L0:	return res;
} /* _tr7 */


int getBattery P(( int _tp1));

static int _tr8( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _rp0;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	_rp0 = getBattery(_tp1);

	res = PREPAbort ?
		WSPutFunction( wslp, "Abort", 0) : WSPutInteger( wslp, _rp0);
L1: 
L0:	return res;
} /* _tr8 */


void getAccelerationWSTP P(( int _tp1));

static int _tr9( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	getAccelerationWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr9 */


void getOrientationWSTP P(( int _tp1));

static int _tr10( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	getOrientationWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr10 */


void getSampledSectionWSTP P(( int _tp1, int _tp2));

static int _tr11( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	int _tp2;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetInteger( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	getSampledSectionWSTP(_tp1, _tp2);

	res = 1;
L2: L1: 
L0:	return res;
} /* _tr11 */


void unloadAllSoundsWSTP P(( void));

static int _tr12( WSLINK wslp)
{
	int	res = 0;
	if ( ! WSNewPacket(wslp) ) goto L0;
	if( !wslp) return res; /* avoid unused parameter warning */

	unloadAllSoundsWSTP();

	res = 1;

L0:	return res;
} /* _tr12 */


void loadSoundWSTP P(( const char * _tp1, const char * _tp2));

static int _tr13( WSLINK wslp)
{
	int	res = 0;
	const char * _tp1;
	const char * _tp2;
	if ( ! WSGetString( wslp, &_tp1) ) goto L0;
	if ( ! WSGetString( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	loadSoundWSTP(_tp1, _tp2);

	res = 1;
L2:	WSReleaseString(wslp, _tp2);
L1:	WSReleaseString(wslp, _tp1);

L0:	return res;
} /* _tr13 */


void playSpeakerSoundWSTP P(( int _tp1, const char * _tp2));

static int _tr14( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	const char * _tp2;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSGetString( wslp, &_tp2) ) goto L1;
	if ( ! WSNewPacket(wslp) ) goto L2;

	playSpeakerSoundWSTP(_tp1, _tp2);

	res = 1;
L2:	WSReleaseString(wslp, _tp2);
L1: 
L0:	return res;
} /* _tr14 */


void stopSpeakerWSTP P(( int _tp1));

static int _tr15( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	stopSpeakerWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr15 */


void getJoystickDataWSTP P(( int _tp1));

static int _tr16( WSLINK wslp)
{
	int	res = 0;
	int _tp1;
	if ( ! WSGetInteger( wslp, &_tp1) ) goto L0;
	if ( ! WSNewPacket(wslp) ) goto L1;

	getJoystickDataWSTP(_tp1);

	res = 1;
L1: 
L0:	return res;
} /* _tr16 */


static struct func {
	int   f_nargs;
	int   manual;
	int   (*f_func)P((WSLINK));
	const char  *f_name;
	} _tramps[17] = {
		{ 1, 0, _tr0, "connectWiimoteWSTP" },
		{ 0, 0, _tr1, "disconnectAllWiimotesWSTP" },
		{ 2, 0, _tr2, "buttonPressed" },
		{ 1, 0, _tr3, "buttonNameWSTP" },
		{ 2, 0, _tr4, "classicButtonPressed" },
		{ 1, 0, _tr5, "classicButtonNameWSTP" },
		{ 2, 0, _tr6, "setLEDsWSTP" },
		{ 2, 0, _tr7, "setRumbleWSTP" },
		{ 1, 0, _tr8, "getBattery" },
		{ 1, 0, _tr9, "getAccelerationWSTP" },
		{ 1, 0, _tr10, "getOrientationWSTP" },
		{ 2, 0, _tr11, "getSampledSectionWSTP" },
		{ 0, 0, _tr12, "unloadAllSoundsWSTP" },
		{ 2, 0, _tr13, "loadSoundWSTP" },
		{ 2, 0, _tr14, "playSpeakerSoundWSTP" },
		{ 1, 0, _tr15, "stopSpeakerWSTP" },
		{ 1, 0, _tr16, "getJoystickDataWSTP" }
		};

static const char* evalstrs[] = {
	"BeginPackage[\"Wiimote`\"]",
	(const char*)0,
	"ConnectWiimote::usage = \"ConnectWiimote[id] tries to pair to the",
	" specified wiimote.\"",
	(const char*)0,
	"DisconnectAll::usage = \"DisconnectAll[] disconnects all active w",
	"iimotes. Use with care.\"",
	(const char*)0,
	"GetButtonName::usage = \"GetButtonName[button] will return the na",
	"me of the specified button bit.\"",
	(const char*)0,
	"GetClassicButtonName::usage = \"GetClassicButtonName[button] will",
	" return the name of the specified Classic Controller button.\"",
	(const char*)0,
	"ButtonPressed::usage = \"ButtonPressed[id, button] will return wh",
	"ether the button is pressed.\"",
	(const char*)0,
	"ClassicButtonPressed::usage = \"ClassicButtonPressed[id, button] ",
	"will return whether the Classic Controller button is pressed.\"",
	(const char*)0,
	"GetJoystickData::usage = \"GetJoystickData[id] will return the jo",
	"ystick position (float[4]) if connected.  If a nunchuk is connec",
	"ted, only first two floats are used. Otherwise, if the extension",
	" is a classic controller,  Guitar Hero Guitar or Drums, it will ",
	"provide both left and right joystick data as four floats.\"",
	(const char*)0,
	"SetLEDs::usage = \"SetLEDs[id, leds] will set the LEDs on the wii",
	"mote appropriately.\"",
	(const char*)0,
	"SetRumble::usage = \"SetRumble[id, rumble] will set the state of ",
	"the wiimote rumble (0 = off).\"",
	(const char*)0,
	"GetBattery::usage = \"GetBattery[id] will return the percentage o",
	"f battery left on the remote.\"",
	(const char*)0,
	"GetAcceleration::usage = \"GetAcceleration[id] will return the ac",
	"celeration (float[6]).  The first three floats will be the wiimo",
	"te acceleration. The last three will be nunchuk acceleration,  i",
	"f one is connected. Otherwise, those accelerations will be 0/0/0",
	".\"",
	(const char*)0,
	"GetOrientation::usage = \"GetOrientation[id] will return the wiim",
	"ote orientation. Format is   float[UpdateAge, Pitch, Roll, Nunch",
	"uk UpdateAge, Nunchuk Pitch, Nunchuk Roll]. If UpdateAge is high",
	",  data is probably invalid, as orientation can only be estimate",
	"d when the acceleration is near-zero.  Nunchuk data only availab",
	"le if that extension is connected. Otherwise, will be zeros for ",
	"all three.\"",
	(const char*)0,
	"GetSampledSection::usage = \"GetSampledSection[id, time] will ret",
	"urn the wiimote acceleration over time.  The argument time is in",
	" tens of milliseconds, and the format returned will be an array ",
	"of floats[time * 3],   representing the (x, y, z) of each sample",
	"d section of time. If any errors occur, the array will fill with",
	" the  update error code (-2 for disconnected, -1 for invalid id,",
	" etc.)\"",
	(const char*)0,
	"UnloadSounds::usage = \"UnloadSounds[] will unload all loaded sou",
	"nds and stop all playing music.\"",
	(const char*)0,
	"LoadSound::usage = \"LoadSound[soundId, path] will pre-load a sou",
	"nd with an identifier.\"",
	(const char*)0,
	"PlaySound::usage = \"PlaySound[id, soundId] will play a pre-loade",
	"d sound on the given wiimote.\"",
	(const char*)0,
	"StopSound::usage = \"StopSound[id] will turn off the speaker (sto",
	"p playing) on the given wiimote.\"",
	(const char*)0,
	"Begin[\"`Private`\"]",
	(const char*)0,
	"End[ ]",
	(const char*)0,
	"EndPackage[ ]",
	(const char*)0,
	(const char*)0
};
#define CARDOF_EVALSTRS 21

static int _definepattern P(( WSLINK, char*, char*, int));

static int _doevalstr P(( WSLINK, int));

int  _PREPDoCallPacket P(( WSLINK, struct func[], int));


int WSInstall( WSLINK wslp)
{
	int _res;
	_res = WSConnect(wslp);
	if (_res) _res = _doevalstr( wslp, 0);
	if (_res) _res = _doevalstr( wslp, 1);
	if (_res) _res = _doevalstr( wslp, 2);
	if (_res) _res = _doevalstr( wslp, 3);
	if (_res) _res = _doevalstr( wslp, 4);
	if (_res) _res = _doevalstr( wslp, 5);
	if (_res) _res = _doevalstr( wslp, 6);
	if (_res) _res = _doevalstr( wslp, 7);
	if (_res) _res = _doevalstr( wslp, 8);
	if (_res) _res = _doevalstr( wslp, 9);
	if (_res) _res = _doevalstr( wslp, 10);
	if (_res) _res = _doevalstr( wslp, 11);
	if (_res) _res = _doevalstr( wslp, 12);
	if (_res) _res = _doevalstr( wslp, 13);
	if (_res) _res = _doevalstr( wslp, 14);
	if (_res) _res = _doevalstr( wslp, 15);
	if (_res) _res = _doevalstr( wslp, 16);
	if (_res) _res = _doevalstr( wslp, 17);
	if (_res) _res = _doevalstr( wslp, 18);
	if (_res) _res = _definepattern(wslp, (char *)"ConnectWiimote[id_Integer]", (char *)"{ id }", 0);
	if (_res) _res = _definepattern(wslp, (char *)"DisconnectAll[]", (char *)"{ }", 1);
	if (_res) _res = _definepattern(wslp, (char *)"ButtonPressed[id_Integer, button_Integer]", (char *)"{ id, button }", 2);
	if (_res) _res = _definepattern(wslp, (char *)"GetButtonName[button_Integer]", (char *)"{ button }", 3);
	if (_res) _res = _definepattern(wslp, (char *)"ClassicButtonPressed[id_Integer, button_Integer]", (char *)"{ id, button }", 4);
	if (_res) _res = _definepattern(wslp, (char *)"GetClassicButtonName[button_Integer]", (char *)"{ button }", 5);
	if (_res) _res = _definepattern(wslp, (char *)"SetLEDs[id_Integer, leds_Integer]", (char *)"{ id, leds }", 6);
	if (_res) _res = _definepattern(wslp, (char *)"SetRumble[id_Integer, rumble_Integer]", (char *)"{ id, rumble }", 7);
	if (_res) _res = _definepattern(wslp, (char *)"GetBattery[id_Integer]", (char *)"{ id }", 8);
	if (_res) _res = _definepattern(wslp, (char *)"GetAcceleration[id_Integer]", (char *)"{ id }", 9);
	if (_res) _res = _definepattern(wslp, (char *)"GetOrientation[id_Integer]", (char *)"{ id }", 10);
	if (_res) _res = _definepattern(wslp, (char *)"GetSampledSection[id_Integer, time_Integer]", (char *)"{ id, time }", 11);
	if (_res) _res = _definepattern(wslp, (char *)"UnloadSounds[]", (char *)"{ }", 12);
	if (_res) _res = _definepattern(wslp, (char *)"LoadSound[soundId_String, path_String]", (char *)"{ soundId, path }", 13);
	if (_res) _res = _definepattern(wslp, (char *)"PlaySound[id_Integer, soundId_String]", (char *)"{ id, soundId }", 14);
	if (_res) _res = _definepattern(wslp, (char *)"StopSound[id_Integer]", (char *)"{ id }", 15);
	if (_res) _res = _definepattern(wslp, (char *)"GetJoystickData[id_Integer]", (char *)"{ id }", 16);
	if (_res) _res = _doevalstr( wslp, 19);
	if (_res) _res = _doevalstr( wslp, 20);
	if (_res) _res = WSPutSymbol( wslp, "End");
	if (_res) _res = WSFlush( wslp);
	return _res;
} /* WSInstall */


int PREPDoCallPacket( WSLINK wslp)
{
	return _PREPDoCallPacket( wslp, _tramps, 17);
} /* PREPDoCallPacket */

/******************************* begin trailer ********************************/

#ifndef EVALSTRS_AS_BYTESTRINGS
#	define EVALSTRS_AS_BYTESTRINGS 1
#endif

#if CARDOF_EVALSTRS
static int  _doevalstr( WSLINK wslp, int n)
{
	long bytesleft, charsleft, bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
	long charsnow;
#endif
	char **s, **p;
	char *t;

	s = (char **)evalstrs;
	while( n-- > 0){
		if( *s == 0) break;
		while( *s++ != 0){}
	}
	if( *s == 0) return 0;
	bytesleft = 0;
	charsleft = 0;
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = (long)(t - *p);
		bytesleft += bytesnow;
		charsleft += bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
		t = *p;
		charsleft -= WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
#endif
		++p;
	}


	WSPutNext( wslp, WSTKSTR);
#if EVALSTRS_AS_BYTESTRINGS
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = (long)(t - *p);
		bytesleft -= bytesnow;
		WSPut8BitCharacters( wslp, bytesleft, (unsigned char*)*p, bytesnow);
		++p;
	}
#else
	WSPut7BitCount( wslp, (long_st)charsleft, (long_st)bytesleft);

	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		t = *p;
		charsnow = bytesnow - WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
		charsleft -= charsnow;
		WSPut7BitCharacters(  wslp, charsleft, *p, bytesnow, charsnow);
		++p;
	}
#endif
	return WSError( wslp) == WSEOK;
}
#endif /* CARDOF_EVALSTRS */


static int  _definepattern( WSLINK wslp, char *patt, char *args, int func_n)
{
	WSPutFunction( wslp, "DefineExternal", (long)3);
	  WSPutString( wslp, patt);
	  WSPutString( wslp, args);
	  WSPutInteger( wslp, func_n);
	return !WSError(wslp);
} /* _definepattern */


int _PREPDoCallPacket( WSLINK wslp, struct func functable[], int nfuncs)
{
	int len;
	int n, res = 0;
	struct func* funcp;

	if( ! WSGetInteger( wslp, &n) ||  n < 0 ||  n >= nfuncs) goto L0;
	funcp = &functable[n];

	if( funcp->f_nargs >= 0
	&& ( ! WSTestHead(wslp, "List", &len)
	     || ( !funcp->manual && (len != funcp->f_nargs))
	     || (  funcp->manual && (len <  funcp->f_nargs))
	   )
	) goto L0;

	stdlink = wslp;
	res = (*funcp->f_func)( wslp);

L0:	if( res == 0)
		res = WSClearError( wslp) && WSPutSymbol( wslp, "$Failed");
	return res && WSEndPacket( wslp) && WSNewPacket( wslp);
} /* _PREPDoCallPacket */


wsapi_packet WSAnswer( WSLINK wslp)
{
	wsapi_packet pkt = 0;
	int waitResult;

	while( ! PREPDone && ! WSError(wslp)
		&& (waitResult = WSWaitForLinkActivity(wslp),waitResult) &&
		waitResult == WSWAITSUCCESS && (pkt = WSNextPacket(wslp), pkt) &&
		pkt == CALLPKT)
	{
		PREPAbort = 0;
		if(! PREPDoCallPacket(wslp))
			pkt = 0;
	}
	PREPAbort = 0;
	return pkt;
}



/*
	Module[ { me = $ParentLink},
		$ParentLink = contents of RESUMEPKT;
		Message[ MessageName[$ParentLink, "notfe"], me];
		me]
*/

static int refuse_to_be_a_frontend( WSLINK wslp)
{
	int pkt;

	WSPutFunction( wslp, "EvaluatePacket", 1);
	  WSPutFunction( wslp, "Module", 2);
	    WSPutFunction( wslp, "List", 1);
		  WSPutFunction( wslp, "Set", 2);
		    WSPutSymbol( wslp, "me");
	        WSPutSymbol( wslp, "$ParentLink");
	  WSPutFunction( wslp, "CompoundExpression", 3);
	    WSPutFunction( wslp, "Set", 2);
	      WSPutSymbol( wslp, "$ParentLink");
	      WSTransferExpression( wslp, wslp);
	    WSPutFunction( wslp, "Message", 2);
	      WSPutFunction( wslp, "MessageName", 2);
	        WSPutSymbol( wslp, "$ParentLink");
	        WSPutString( wslp, "notfe");
	      WSPutSymbol( wslp, "me");
	    WSPutSymbol( wslp, "me");
	WSEndPacket( wslp);

	while( (pkt = WSNextPacket( wslp), pkt) && pkt != SUSPENDPKT)
		WSNewPacket( wslp);
	WSNewPacket( wslp);
	return WSError( wslp) == WSEOK;
}


int PREPEvaluate( WSLINK wslp, char *s)
{
	if( PREPAbort) return 0;
	return WSPutFunction( wslp, "EvaluatePacket", 1L)
		&& WSPutFunction( wslp, "ToExpression", 1L)
		&& WSPutString( wslp, s)
		&& WSEndPacket( wslp);
}


int PREPEvaluateString( WSLINK wslp, char *s)
{
	int pkt;
	if( PREPAbort) return 0;
	if( PREPEvaluate( wslp, s)){
		while( (pkt = WSAnswer( wslp), pkt) && pkt != RETURNPKT)
			WSNewPacket( wslp);
		WSNewPacket( wslp);
	}
	return WSError( wslp) == WSEOK;
} /* PREPEvaluateString */


void PREPDefaultHandler( WSLINK wslp, int message, int n)
{
	wslp = (WSLINK)0; /* suppress unused warning */
	n = 0;          /* suppress unused warning */

	switch (message){
	case WSTerminateMessage:
		PREPDone = 1;
	case WSInterruptMessage:
	case WSAbortMessage:
		PREPAbort = 1;
	default:
		return;
	}
}



static int _WSMain( char **argv, char **argv_end, char *commandline)
{
	WSLINK wslp;
	int err;

	if( !stdenv)
		stdenv = WSInitialize( (WSEnvironmentParameter)0);

	if( stdenv == (WSEnvironment)0) goto R0;

	if( !stdyielder)
		stdyielder = (WSYieldFunctionObject)PREPDefaultYielder;

	if( !stdhandler)
		stdhandler = (WSMessageHandlerObject)PREPDefaultHandler;

	wslp = commandline
		? WSOpenString( stdenv, commandline, &err)
		: WSOpenArgcArgv( stdenv, (int)(argv_end - argv), argv, &err);
	if( wslp == (WSLINK)0){
		WSAlert( stdenv, WSErrorString( stdenv, err));
		goto R1;
	}

	if( WSIconWindow){
#define TEXTBUFLEN 64
		TCHAR textbuf[TEXTBUFLEN];
		PTCHAR tmlname;
		const char *mlname;
		size_t namelen, i;
		int len;
		len = GetWindowText(WSIconWindow, textbuf, 62 );
		mlname = WSName(wslp);
		namelen = strlen(mlname);
		tmlname = (PTCHAR)malloc((namelen + 1)*sizeof(TCHAR));
		if(tmlname == NULL) goto R2;

		for(i = 0; i < namelen; i++){
			tmlname[i] = mlname[i];
		}
		tmlname[namelen] = '\0';
		
#if defined(_MSC_VER) && (_MSC_VER >= 1400)
		_tcscat_s( textbuf + len, TEXTBUFLEN - len, __TEXT("("));
		_tcsncpy_s(textbuf + len + 1, TEXTBUFLEN - len - 1, tmlname, TEXTBUFLEN - len - 3);
		textbuf[TEXTBUFLEN - 2] = '\0';
		_tcscat_s(textbuf, TEXTBUFLEN, __TEXT(")"));
#else
		_tcscat( textbuf + len, __TEXT("("));
		_tcsncpy( textbuf + len + 1, tmlname, TEXTBUFLEN - len - 3);
		textbuf[TEXTBUFLEN - 2] = '\0';
		_tcscat( textbuf, __TEXT(")"));
#endif
		textbuf[len + namelen + 2] = '\0';
		free(tmlname);
		SetWindowText( WSIconWindow, textbuf);
	}

	if( WSInstance){
		if( stdyielder) WSSetYieldFunction( wslp, stdyielder);
		if( stdhandler) WSSetMessageHandler( wslp, stdhandler);
	}

	if( WSInstall( wslp))
		while( WSAnswer( wslp) == RESUMEPKT){
			if( ! refuse_to_be_a_frontend( wslp)) break;
		}

R2:	WSClose( wslp);
R1:	WSDeinitialize( stdenv);
	stdenv = (WSEnvironment)0;
R0:	return !PREPDone;
} /* _WSMain */


int WSMainString( char *commandline)
{
	return _WSMain( (char **)0, (char **)0, commandline);
}

int WSMainArgv( char** argv, char** argv_end) /* note not FAR pointers */
{   
	static char FAR * far_argv[128];
	int count = 0;
	
	while(argv < argv_end)
		far_argv[count++] = *argv++;
		 
	return _WSMain( far_argv, far_argv + count, (char *)0);
}

int WSMain( int argc, char **argv)
{
 	return _WSMain( argv, argv + argc, (char *)0);
}
 
